//
//  ViewController.swift
//  IOSProject_IvanBarrantes_101275771
//
//  Created by Ivan Barrantes on 2020-04-15.
//  Copyright © 2020 Ivan Barrantes. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var dobPicker: UIDatePicker!
    @IBOutlet weak var switchTravel: UISwitch!
    
    let calendar = Calendar.current
    var peopleP1:[String] = []
    var peopleP2:[String] = []
    var peopleP3:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("Here it starts")
        print(peopleP1)
        print(peopleP2)
        print(peopleP2)
        //textView.text = text
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //textView.text = text
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        dobPicker.datePickerMode = .date
   
        let userInput = calendar.dateComponents([.day,.month,.year], from: dobPicker.date)
        
        let newPatient = Patient(name: nameField.text!, age: getAge(year: userInput.year!, month: userInput.month!, day: userInput.day!), travelH: switchTravel.isOn)
        
        let statusScreen = segue.destination as! TabBarController
        
        statusScreen.nameFrom1 = newPatient.name
        newPatient.calPriority()
        statusScreen.priorityFrom1 = "\(newPatient.priority)"
        statusScreen.ageFrom1 = "\(newPatient.age)"
        
        switch newPatient.priority {
        case 0:
            print("no need test label to adad")
        case 1:
            peopleP1.append(newPatient.name)
            statusScreen.test = peopleP1
        case 2:
            peopleP2.append(newPatient.name)
            statusScreen.test2 = peopleP2
        case 3:
            peopleP3.append(newPatient.name)
            statusScreen.test3 = peopleP3
        default:
            break;
        }
        
       
    }
    
    func getAge(year:Int, month:Int, day:Int) -> Int {
        var age:Int
        var numberDay:Double
        var numberCurrent:Double
        let currentDate = calendar.dateComponents([.day,.month,.year], from: Date())
        age = currentDate.year! - year
        numberDay = (Double(month)*30.5)+Double(day)
        numberCurrent = (Double(currentDate.month!)*30.5)+Double(currentDate.day!)
        if numberCurrent < numberDay {
            age -= 1
        } else {
            return age
        }
        return age
    }

}

