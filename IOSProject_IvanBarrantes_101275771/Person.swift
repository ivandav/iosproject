//
//  Person.swift
//  IOSProject_IvanBarrantes_101275771
//
//  Created by Ivan Barrantes on 2020-04-15.
//  Copyright © 2020 Ivan Barrantes. All rights reserved.
//

import Foundation

class Patient {
    var name:String
    var age:Int
    var travelH:Bool
    var priority:Int = 0
    var numberOnList:Int?
    
    init(name:String, age:Int, travelH:Bool) {
        self.name = name
        self.age = age
        self.travelH = travelH
    }
    
    func calPriority () {
        if age > 65 && travelH {
            self.priority = 3
        } else if age > 65 {
            self.priority = 2
        } else if travelH {
            self.priority = 1
        }
    }
}
