//
//  WaitingTableController.swift
//  IOSProject_IvanBarrantes_101275771
//
//  Created by Ivan Barrantes on 2020-04-15.
//  Copyright © 2020 Ivan Barrantes. All rights reserved.
//

import UIKit

class WaitingTableController: UITableViewController {
    
    var data1:[String] = []
    var data2:[String] = []
    var data3:[String] = []
    
    var currentPerson:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0:
            return data1.count
        case 1:
            return data2.count
        case 2:
            return data3.count
        default:
            break;
        }
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        switch indexPath.section {
        case 0:
            cell.textLabel?.text = data1[indexPath.row]
        case 1:
            cell.textLabel?.text = data2[indexPath.row]
        case 2:
            cell.textLabel?.text = data3[indexPath.row]
        default:
            break;
        }
        

        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let box = UIAlertController(title: "", message: "Is their test done?", preferredStyle: .alert)
        box.addAction(UIAlertAction(title: "NO", style: .default , handler: nil))
        box.addAction(UIAlertAction(title: "YES", style: .default , handler: {
            action in
            
            switch indexPath.section {
            case 0:
                self.data1.remove(at: indexPath.row)
            case 1:
                self.data2.remove(at: indexPath.row)
            case 2:
                self.data3.remove(at: indexPath.row)
            default:
                break;
            }
            
        }))
        switch indexPath.section {
        case 0:
            currentPerson = data1[indexPath.row]
            box.title = currentPerson
        case 1:
            currentPerson = data2[indexPath.row]
            box.title = currentPerson
        case 2:
            currentPerson = data3[indexPath.row]
            box.title = currentPerson
        default:
            break;
        }
        self.present(box, animated: true)
        
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
    
    }
    

}
